﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FolhaOO_EF_Console.Model;
using FolhaOO_EF_Console.DAO;
using FolhaOO_EF_Console.Negocio;
namespace FolhaOO_EF_Console.View
{
    class Program
    {
        static void Main(string[] args)
        {
            int opc;
            do
            {
                Console.WriteLine("\n\n*** FOLHA DE PAGAMENTO ***");
                Console.WriteLine("\n1 - Funcionários");
                Console.WriteLine("2 - Folhas de Pagamento");
                Console.WriteLine("3 - Sair");
                Console.Write("Opção: ");
                opc = int.Parse(Console.ReadLine());
                switch (opc)
                {
                    case 1:
                        MenuFuncionarios();
                        break;
                    case 2:
                        MenuFolhas();
                        break;
                }
            } while (opc != 3);

        }

        static void MenuFuncionarios()
        {
            int opc;
            do
            {
                Console.WriteLine("\n\n*** FUNCIONÁRIOS ***");
                Console.WriteLine("\n1 - Incluir");
                Console.WriteLine("2 - Consultar");
                Console.WriteLine("3 - Alterar");
                Console.WriteLine("4 - Excluir");
                Console.WriteLine("5 - Listar");
                Console.WriteLine("6 - Voltar");
                Console.Write("Opção: ");
                opc = int.Parse(Console.ReadLine());
                switch (opc)
                {
                    case 1:
                        IncluirFuncionario();
                        break;
                    case 2:
                        ConsultarFuncionarios();
                        break;
                    case 3:
                        AlterarFuncionarios();
                        break;
                    case 4:
                        ExcluirFuncionarios();
                        break;
                    case 5:
                        ListarFuncionarios();
                        break;
                }
            } while (opc != 6);            
        }

        static void MenuFolhas()
        {
            int opc;
            do
            {
                Console.WriteLine("\n\n*** FOLHAS DE PAGAMENTO ***");
                Console.WriteLine("\n1 - Incluir");
                Console.WriteLine("2 - Consultar");
                Console.WriteLine("3 - Listar");
                Console.WriteLine("4 - Voltar");
                Console.Write("Opção: ");
                opc = int.Parse(Console.ReadLine());
                switch (opc)
                {
                    case 1:
                        IncluirFolha();
                        break;
                    case 2:
                        ConsultarFolha();
                        break;
                    case 3:
                        ListarFolhas();
                        break;
                }
            } while (opc != 4);            
        }

        //Método para inclusão de funcionários
        static void IncluirFuncionario()
        {
            Funcionario Funcionario = new Funcionario();
            Console.Write("\n\nInforme o CPF: ");
            Funcionario.Cpf = Console.ReadLine();
            if (FuncionarioNegocio.ValidarCpf(Funcionario))
            {
                Console.Write("Nome: ");
                Funcionario.Nome = Console.ReadLine();
                if (FuncionarioDAO.ObterFuncionarioPorCpf(Funcionario) == null)
                {
                    if (FuncionarioDAO.Incluir(Funcionario))
                    {
                        Console.WriteLine("Operação bem sucedida");
                    }
                    else
                    {
                        Console.WriteLine("A operação não pode ser realizada");
                    }
                }
                else
                {
                    Console.WriteLine("Funcionário já cadastrado");
                }
            }
            else
            {
                Console.WriteLine("CPF inválido");
            }
            Console.ReadKey();
        }

        //Método para consultar funcionários
        static void ConsultarFuncionarios()
        {
            int opc;
            Funcionario Funcionario = new Funcionario();
            Console.WriteLine("\n\n1 - Consultar por CPF");
            Console.WriteLine("2 - Consultar por Nome");
            Console.Write("Opção: ");
            opc = int.Parse(Console.ReadLine());
            if (opc == 1)
            {
                Console.Write("Informe o CPF: ");
                Funcionario.Cpf = Console.ReadLine();
                Funcionario = FuncionarioDAO.ObterFuncionarioPorCpf(Funcionario);
            }
            else
            {
                Console.Write("Informe o nome: ");
                Funcionario.Nome = Console.ReadLine();
                Funcionario = FuncionarioDAO.ObterFuncionarioPorNome(Funcionario);
            }
            if (Funcionario != null)
            {
                Console.WriteLine("\n\nID: " + Funcionario.Id);
                Console.WriteLine("Nome: " + Funcionario.Nome);
                Console.WriteLine("CPF: " + Funcionario.Cpf);
            }
            else
            {
                Console.WriteLine("\n\nFuncionário não cadastrado");
            }
            Console.ReadKey();
        }

        //Método para alterar funcionários
        static void AlterarFuncionarios()
        {
            int opc;
            string resp;
            Funcionario Funcionario = new Funcionario();
            Console.WriteLine("\n\n1 - Consultar por CPF");
            Console.WriteLine("2 - Consultar por Nome");
            Console.Write("Opção: ");
            opc = int.Parse(Console.ReadLine());
            if (opc == 1)
            {
                Console.Write("Informe o CPF: ");
                Funcionario.Cpf = Console.ReadLine();
                Funcionario = FuncionarioDAO.ObterFuncionarioPorCpf(Funcionario);
            }
            else
            {
                Console.Write("Informe o nome: ");
                Funcionario.Nome = Console.ReadLine();
                Funcionario = FuncionarioDAO.ObterFuncionarioPorNome(Funcionario);
            }
            if (Funcionario != null)
            {
                Console.WriteLine("\n\nID: " + Funcionario.Id);
                Console.WriteLine("Nome: " + Funcionario.Nome);
                Console.WriteLine("CPF: " + Funcionario.Cpf);
                Console.Write("Confirma a alteração? ");
                resp = Console.ReadLine();
                if (resp.Equals("s") || resp.Equals("S"))
                {
                    Console.Write("Novo nome: ");
                    Funcionario.Nome = Console.ReadLine();
                    if (FuncionarioDAO.Alterar(Funcionario))
                    {
                        Console.WriteLine("Operação bem sucedida");
                    }
                    else
                    {
                        Console.WriteLine("A operação não pode ser realizada");
                    }
                }
            }
            else
            {
                Console.WriteLine("\n\nFuncionário não cadastrado");
            }
            Console.ReadKey();
        }

        //Método para excluir funcionários
        static void ExcluirFuncionarios()
        {
            int opc;
            string resp;
            Funcionario Funcionario = new Funcionario();
            Console.WriteLine("\n\n1 - Consultar por CPF");
            Console.WriteLine("2 - Consultar por Nome");
            Console.Write("Opção: ");
            opc = int.Parse(Console.ReadLine());
            if (opc == 1)
            {
                Console.Write("Informe o CPF: ");
                Funcionario.Cpf = Console.ReadLine();
                Funcionario = FuncionarioDAO.ObterFuncionarioPorCpf(Funcionario);
            }
            else
            {
                Console.Write("Informe o nome: ");
                Funcionario.Nome = Console.ReadLine();
                Funcionario = FuncionarioDAO.ObterFuncionarioPorNome(Funcionario);
            }
            if (Funcionario != null)
            {
                Console.WriteLine("\n\nID: " + Funcionario.Id);
                Console.WriteLine("Nome: " + Funcionario.Nome);
                Console.WriteLine("CPF: " + Funcionario.Cpf);
                Console.Write("Confirma a exclusão? ");
                resp = Console.ReadLine();
                if (resp.Equals("s") || resp.Equals("S"))
                {
                    if (FuncionarioDAO.Excluir(Funcionario))
                    {
                        Console.WriteLine("Operação bem sucedida");
                    }
                    else
                    {
                        Console.WriteLine("A operação não pode ser realizada");
                    }
                }
            }
            else
            {
                Console.WriteLine("\n\nFuncionário não cadastrado");
            }
            Console.ReadKey();
        }

        //Método para listar os funcionários cadastrados
        static void ListarFuncionarios()
        {
            Console.WriteLine("\n\nLista dos funcionários");
            foreach (Funcionario i in FuncionarioDAO.ObterFuncionarios())
            {
                Console.WriteLine("ID: " + i.Id);
                Console.WriteLine("Nome: " + i.Nome);
                Console.WriteLine("CPF: " + i.Cpf);
                Console.WriteLine("----------------------------------->");
            }
        }

        //Método para incluir folhas de pagamento
        static void IncluirFolha()
        {
            int opc;
            string resp;
            Funcionario Funcionario = new Funcionario();
            Folha Folha = new Folha();
            Console.WriteLine("\n\nSelecionar o funcionário...");
            Console.WriteLine("1 - Consultar por CPF");
            Console.WriteLine("2 - Consultar por Nome");
            Console.Write("Opção: ");
            opc = int.Parse(Console.ReadLine());
            if (opc == 1)
            {
                Console.Write("Informe o CPF: ");
                Funcionario.Cpf = Console.ReadLine();
                Funcionario = FuncionarioDAO.ObterFuncionarioPorCpf(Funcionario);
            }
            else
            {
                Console.Write("Informe o nome: ");
                Funcionario.Nome = Console.ReadLine();
                Funcionario = FuncionarioDAO.ObterFuncionarioPorNome(Funcionario);
            }
            if (Funcionario != null)
            {
                Console.WriteLine("\n\nID: " + Funcionario.Id);
                Console.WriteLine("Nome: " + Funcionario.Nome);
                Console.WriteLine("CPF: " + Funcionario.Cpf);
                Console.Write("Confirma o funcionário selecionado? ");
                resp = Console.ReadLine();
                if (resp.Equals("s") || resp.Equals("S"))
                {
                    Folha.Funcionario = Funcionario;
                    Console.Write("Informe o mês: ");
                    Folha.Mes = int.Parse(Console.ReadLine());
                    Console.Write("Informe o ano: ");
                    Folha.Ano = int.Parse(Console.ReadLine());
                    if (FolhaDAO.ObterFolha(Folha) == null)
                    {
                        Console.Write("Informe o número de horas trabalhadas: ");
                        Folha.Horas = int.Parse(Console.ReadLine());
                        Console.Write("Informe o valor da hora: ");
                        Folha.Valor = float.Parse(Console.ReadLine());
                        if (FolhaDAO.Incluir(Folha))
                        {
                            Console.WriteLine("Operação bem sucedida");
                        }
                        else
                        {
                            Console.WriteLine("A operação não pode ser realizada");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Folha de pagamento já cadastrada");
                    }
                }
            }
            else
            {
                Console.WriteLine("\n\nFuncionário não cadastrado");
            }
            Console.ReadKey();
        }

        //Método para consultar a folha de pagamento
        static void ConsultarFolha()
        {
            int opc;
            string resp;
            Funcionario Funcionario = new Funcionario();
            Folha Folha = new Folha();
            Console.WriteLine("\n\nSelecionar o funcionário...");
            Console.WriteLine("1 - Consultar por CPF");
            Console.WriteLine("2 - Consultar por Nome");
            Console.Write("Opção: ");
            opc = int.Parse(Console.ReadLine());
            if (opc == 1)
            {
                Console.Write("Informe o CPF: ");
                Funcionario.Cpf = Console.ReadLine();
                Funcionario = FuncionarioDAO.ObterFuncionarioPorCpf(Funcionario);
            }
            else
            {
                Console.Write("Informe o nome: ");
                Funcionario.Nome = Console.ReadLine();
                Funcionario = FuncionarioDAO.ObterFuncionarioPorNome(Funcionario);
            }
            if (Funcionario != null)
            {
                Console.WriteLine("\n\nID: " + Funcionario.Id);
                Console.WriteLine("Nome: " + Funcionario.Nome);
                Console.WriteLine("CPF: " + Funcionario.Cpf);
                Console.Write("Confirma o funcionário selecionado? ");
                resp = Console.ReadLine();
                if (resp.Equals("s") || resp.Equals("S"))
                {
                    Folha.Funcionario = Funcionario;
                    Console.Write("Informe o mês: ");
                    Folha.Mes = int.Parse(Console.ReadLine());
                    Console.Write("Informe o ano: ");
                    Folha.Ano = int.Parse(Console.ReadLine());
                    Folha = FolhaDAO.ObterFolha(Folha);
                    if (Folha != null)
                    {
                        Console.WriteLine("Número de horas trabalhadas: " + Folha.Horas);
                        Console.WriteLine("Valor da hora: " + Folha.Valor);                        
                        Console.WriteLine("Salário bruto: " + FolhaNegocio.CalcularSalarioBruto(Folha).ToString("c2"));
                        Console.WriteLine("IR: " + FolhaNegocio.CalcularIR(Folha).ToString("c2"));
                        Console.WriteLine("INSS: " + FolhaNegocio.CalcularINSS(Folha).ToString("c2"));
                        Console.WriteLine("FGTS: " + FolhaNegocio.CalcularFGTS(Folha).ToString("c2"));
                        Console.WriteLine("Salário líquido: " + FolhaNegocio.CalcularSalarioLiquido(Folha).ToString("c2"));
                    }
                    else
                    {
                        Console.WriteLine("Folha de pagamento não cadastrada");
                    }
                }
            }
            else
            {
                Console.WriteLine("\n\nFuncionário não cadastrado");
            }
            Console.ReadKey();
        }

        //Métodos para listar as folhas de pagamento
        static void ListarFolhas()
        {
            float total = 0;
            Folha Folha = new Folha();
            Console.Write("\n\nMês: ");
            Folha.Mes = int.Parse(Console.ReadLine());
            Console.Write("Ano: ");
            Folha.Ano = int.Parse(Console.ReadLine());
            foreach (Folha x in FolhaDAO.ObterFolhas(Folha))
            {
                Console.WriteLine("Funcionário: " + x.Funcionario.Nome);
                total += FolhaNegocio.CalcularSalarioLiquido(x);
            }
            Console.WriteLine("------------------------------------");
            Console.WriteLine("Total de salários: " + total.ToString("C2"));
            Console.ReadKey();
        }
    }
}
    

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaOO_EF_Console.Model
{
    class Funcionario
    {
        public int Id { set; get; }
        public string Nome { set; get; }
        public string Cpf { set; get; }        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaOO_EF_Console.Model
{
    class Folha
    {
        public int Id { set; get; }
        public virtual Funcionario Funcionario { set; get; }
        public int Mes {set; get;}
        public int Ano { set; get; }
        public int Horas { set; get; }
        public double Valor { set; get; }
    }
}

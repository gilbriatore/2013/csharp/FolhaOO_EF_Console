﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FolhaOO_EF_Console.Model;
namespace FolhaOO_EF_Console.DAO
{
    class FolhaDAO
    {
        public static bool Incluir(Folha Folha)
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Folhas.Add(Folha);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static Folha ObterFolha(Folha Folha)
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                Folha = db.Folhas.FirstOrDefault(x => x.Funcionario.Id == Folha.Funcionario.Id && x.Mes == Folha.Mes && x.Ano == Folha.Ano);
                return Folha;
            }
            catch
            {
                return null;
            }
        }

        public static IOrderedEnumerable<Folha> ObterFolhas(Folha Folha)
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            IOrderedEnumerable<Folha> Folhas = db.Folhas.Include("Funcionario").Where(x => x.Mes == Folha.Mes && x.Ano == Folha.Ano).ToList().OrderBy(x => x.Mes);
            return Folhas;
        }
    }    
}

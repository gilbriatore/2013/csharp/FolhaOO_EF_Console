﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using FolhaOO_EF_Console.Model;
namespace FolhaOO_EF_Console.DAO
{
    class FolhaEntities : DbContext
    {
        public DbSet<Funcionario> Funcionarios {set; get;}
        public DbSet<Folha> Folhas { set; get; }
    }
}

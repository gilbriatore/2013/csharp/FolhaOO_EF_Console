﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FolhaOO_EF_Console.Model;
using System.Data.Entity;
using System.Data;
namespace FolhaOO_EF_Console.DAO
{
    class FuncionarioDAO
    {
        public static Funcionario ObterFuncionarioPorCpf(Funcionario Funcionario)
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                Funcionario = db.Funcionarios.FirstOrDefault(x => x.Cpf.Equals(Funcionario.Cpf));
                return Funcionario;
            }
            catch
            {
                return null;
            }
        }
        public static Funcionario ObterFuncionarioPorNome(Funcionario Funcionario)
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                Funcionario = db.Funcionarios.FirstOrDefault(x => x.Nome.Equals(Funcionario.Nome));
                return Funcionario;
            }
            catch
            {
                return null;
            }
        }
        public static Funcionario ObterFuncionarioPorId(Funcionario Funcionario)
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                Funcionario = db.Funcionarios.FirstOrDefault(x => x.Id == Funcionario.Id);
                return Funcionario;
            }
            catch
            {
                return null;
            }
        }
        public static bool Incluir(Funcionario Funcionario)
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Funcionarios.Add(Funcionario);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool Alterar(Funcionario Funcionario)
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Entry(Funcionario).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool Excluir(Funcionario Funcionario)
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Funcionarios.Remove(Funcionario);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static IOrderedEnumerable<Funcionario> ObterFuncionarios()
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                IOrderedEnumerable<Funcionario> Funcionarios = db.Funcionarios.ToList().OrderBy(x => x.Nome);
                return Funcionarios;
            }
            catch
            {
                return null;
            }
        }
    }
}
